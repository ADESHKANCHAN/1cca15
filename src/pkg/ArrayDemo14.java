package pkg;

import java.util.Scanner;

public class ArrayDemo14 {
    public static void main(String[] args) {
        Scanner sc1 =new Scanner(System.in);
        System.out.println("ENTER THE TOTAL NO OF FLOORS");
        int floors = sc1.nextInt();
        System.out.println("ENTER THE NO OF TOTAL NO OF FLATS ON EACH FLOOR");
        int flats = sc1.nextInt();

        int  [] [] data = new int [floors] [flats];
        System.out.println("ENTER"+(floors*flats)+"FLAT NOS");

        //accept flat nos
        for(int a=0;a< floors;a++)
        {
            for(int b=0;b<flats;b++)
            {
                data[a] [b] = sc1.nextInt();
            }
        }
        System.out.println("====================");
        for(int a=0;a<floors;a++)
        {
            System.out.println("FLOORS NO"+(a+1));
            System.out.println("================");
            for(int b=0;b<flats;b++)
            {
                System.out.println("FLATS NO:"+data[a] [b] + "\t");
            }
            System.out.println();
            System.out.println("==================");
        }

    }
}
