package pkg;

public class BasicCalculation {
    public static void main(String[] args) {
        int lines=10;
        int star=1;
        for(int row=0;row<=lines;row++) {
            System.out.print("*" );
            for(int col=0;col<=star;col++) {
                System.out.print("*" );
            }
            System.out.println();
            star++;
        }
    }
}
